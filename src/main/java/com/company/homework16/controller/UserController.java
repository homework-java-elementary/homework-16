package com.company.homework16.controller;

import com.company.homework16.entity.User;
import com.company.homework16.service.UserService;
import com.company.homework16.core.annotation.Controller;
import com.company.homework16.mvc.RequestMapping;
import com.company.homework16.mvc.RequestMethod;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final ObjectMapper objectMapper;

    @RequestMapping(url = "/user/get", method = RequestMethod.GET)
    @SneakyThrows
    public void doGetUsers(HttpServletRequest request, HttpServletResponse response) {
        List<User> users = userService.getUsers();
        response.setContentType("application/json");
        objectMapper.writeValue(response.getOutputStream(), users);
    }

    @RequestMapping(url = "/user/post", method = RequestMethod.POST)
    public void doPostUsers(HttpServletRequest request, HttpServletResponse response) {
        try {
            Map<String, String[]> parameters = request.getParameterMap();
            String login = parameters.get("login")[0];
            String password = parameters.get("password")[0];
            boolean result = userService.addUser(login, password);
            response.setContentType("application/json");
            objectMapper.writeValue(response.getOutputStream(), result);
        } catch (Exception e) {
            try {
                response.getOutputStream().write("Parameters is empty".getBytes(StandardCharsets.UTF_8));
                return;
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            return;
        }

    }
}
