package com.company.homework16.core;

import com.company.homework16.core.annotation.Bean;
import com.company.homework16.core.annotation.Configuration;
import com.company.homework16.core.db.JDBCTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    @Bean
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/hw15");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("1234");
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public JDBCTemplate jdbcTemplate(DataSource dataSource) {
        return new JDBCTemplate(dataSource);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
