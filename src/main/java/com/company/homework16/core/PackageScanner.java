package com.company.homework16.core;

import lombok.SneakyThrows;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PackageScanner {
    @SneakyThrows
    public List<Class<?>> findAll(String packageName) {
        String path = packageName.replace(".", "/");
        URL rs = getClass().getClassLoader().getResource(path);
        List<Class<?>> classes = new ArrayList<>();
        File packageDir = new File(rs.toURI());
        for (File file : packageDir.listFiles()) {
            String name = file.getName();
            if (file.isFile() && name.endsWith(".class")) {
                String className = getClassName(packageName, name);
                classes.add(Class.forName(className));
            } else if (file.isDirectory()) {
                List<Class<?>> innerClassses = findAll(packageName + "." + name);
                classes.addAll(innerClassses);
            }
        }
        return classes;
    }

    public List<Class<?>> findWithAnnotation(String packageName, Class<? extends Annotation> annotation) {
        return findAll(packageName).stream()
                .filter(c -> c.isAnnotationPresent(annotation))
                .collect(Collectors.toList());
    }

    private String getClassName(String packageName, String fileName) {
        return packageName + "." + fileName.substring(0, fileName.length() - 6);
    }
}
