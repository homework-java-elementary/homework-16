package com.company.homework16.core;

import com.company.homework16.core.annotation.Component;
import com.company.homework16.core.annotation.Configuration;
import com.company.homework16.core.annotation.Controller;
import com.company.homework16.core.definition.AggregateBeanDefinitionReader;
import com.company.homework16.core.definition.AnnotationBeanDefinitionReader;
import com.company.homework16.core.definition.BeanDefinitionReader;
import com.company.homework16.core.definition.ConfigurationBeanDefinitionReader;
import com.company.homework16.core.postprocess.BeanPostProcessor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class ApplicationInitializer {
    private final PackageScanner packageScanner;

    public Context createContext(String packageName) {
        Context context = new Context();

        BeanDefinitionReader beanDefinitionReader = createBeanDefinitionReader(packageName);
        BeanFactory beanFactory = new BeanFactory(
                context,
                beanDefinitionReader.getBeanDefinitions()
        );
        beanFactory.createAllBeans();

        List<BeanPostProcessor> beanPostProcessors = context.getBeans(BeanPostProcessor.class);
        context.getBeans().forEach(bean ->
                beanPostProcessors.forEach(bpp -> bpp.postProcess(context, bean))
        );

        return context;
    }

    private AggregateBeanDefinitionReader createBeanDefinitionReader(String packageName) {
        BeanDefinitionReader annotationBeanDefinitionReader = new AnnotationBeanDefinitionReader(
                List.of(
                        Configuration.class,
                        Component.class,
                        Controller.class
                ),
                packageScanner,
                packageName
        );
        BeanDefinitionReader configurationBeanDefinitionReader = new ConfigurationBeanDefinitionReader(
                packageScanner,
                packageName
        );
        return new AggregateBeanDefinitionReader(
                annotationBeanDefinitionReader,
                configurationBeanDefinitionReader
        );
    }

    public Context createContext(Class clazz) {
        return createContext(clazz.getPackageName());
    }
}
