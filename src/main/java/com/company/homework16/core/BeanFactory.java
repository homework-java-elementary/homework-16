package com.company.homework16.core;

import com.company.homework16.core.definition.BeanDefinition;
import com.company.homework16.core.definition.BeanDefinitionType;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

@RequiredArgsConstructor
public class BeanFactory {
    private final Context context;
    private final List<BeanDefinition> definitions;

    private void createBean(BeanDefinition definition) {
        if (context.getBean(definition.getId()) != null) {
            return;
        }

        if (definition.getDefinitionType() == BeanDefinitionType.CONSTRUCTOR) {
            createBeanByConstructor(definition);
        } else if (definition.getDefinitionType() == BeanDefinitionType.FACTORY_METHOD) {
            createBeanByFactoryMethod(definition);
        } else {
            throw new RuntimeException("invalid bean definition");
        }
    }

    private void createBeanByFactoryMethod(BeanDefinition definition) {
        Method method = definition.getMethod();
        Object factory = resolveObject(definition.getFactoryClass());
        Object[] arguments = resolveArguments(method.getParameterTypes());

        try {
            Object bean = method.invoke(factory, arguments);
            context.registerBean(definition.getId(), bean);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void createBeanByConstructor(BeanDefinition definition) {
        Constructor[] constructors = definition.getClazz().getConstructors();
        if (constructors.length != 1) {
            throw new RuntimeException("bean class must have only one constructor");
        }
        Constructor constructor = constructors[0];

        Object[] args = resolveArguments(constructor.getParameterTypes());

        try {
            Object bean = constructor.newInstance(args);
            context.registerBean(definition.getId(), bean);
        } catch (Exception e) {
            throw new RuntimeException("bean didn't create", e);
        }
    }

    private Object[] resolveArguments(Class[] parameterTypes) {
        Object[] arguments = new Object[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++) {
            arguments[i] = resolveObject(parameterTypes[i]);
        }
        return arguments;
    }

    private Object resolveObject(Class parameterType) {
        BeanDefinition beanDefinition = findBeanDefinition(parameterType);
        createBean(beanDefinition);
        return context.getBean(beanDefinition.getId());
    }

    private BeanDefinition findBeanDefinition(Class<?> type) {
        return definitions.stream()
                .filter(d -> type.isAssignableFrom(d.getClazz()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Bean not found by type" + type.getName()));
    }

    public void createAllBeans() {
        definitions.forEach(this::createBean);
    }
}
