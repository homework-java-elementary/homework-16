package com.company.homework16.core;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Context {
    private Map<String, Object> beans = new HashMap<>();

    public void registerBean(String beanId, Object bean) {
        beans.put(beanId, bean);
    }

    public Object getBean(String beanId) {
        return beans.get(beanId);
    }

    public <T> T getBean(Class<T> clazz) {
        return beans.values().stream()
                .filter(e -> clazz.isAssignableFrom(e.getClass()))
                .map(o -> (T) o)
                .findFirst()
                .orElse(null);
    }

    public <T> List<T> getBeans(Class<T> clazz) {
        return beans.values().stream()
                .filter(e -> clazz.isAssignableFrom(e.getClass()))
                .map(o -> (T) o)
                .collect(Collectors.toList());
    }

    public List<Object> getBeanByAnnotation(Class<? extends Annotation> clazz) {
        return beans.values().stream()
                .filter(e -> e.getClass().isAnnotationPresent(clazz))
                .collect(Collectors.toList());
    }

    public Collection<Object> getBeans() {
        return beans.values();
    }
}
