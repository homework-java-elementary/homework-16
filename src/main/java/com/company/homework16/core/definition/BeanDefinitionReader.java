package com.company.homework16.core.definition;

import java.util.List;

public interface BeanDefinitionReader {
    List<BeanDefinition> getBeanDefinitions();
}
