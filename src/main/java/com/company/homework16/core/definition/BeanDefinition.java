package com.company.homework16.core.definition;

import com.company.homework16.core.annotation.Bean;
import lombok.Value;

import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Optional;

@Value
public class BeanDefinition {
    BeanDefinitionType definitionType;
    Class factoryClass;
    Method method;
    Class clazz;
    String id;

    public static BeanDefinition create(Method method) {
        String beanId = Optional.of(method)
                .map(m -> m.getAnnotation(Bean.class))
                .map(Bean::id)
                .filter(id -> !id.isEmpty())
                .orElse(method.getName());

        return new BeanDefinition(
                BeanDefinitionType.FACTORY_METHOD,
                method.getDeclaringClass(),
                method,
                method.getReturnType(),
                beanId
        );
    }

    public static BeanDefinition create(Class clazz) {
        return new BeanDefinition(
                BeanDefinitionType.CONSTRUCTOR,
                null,
                null,
                clazz,
                clazz.getSimpleName().toLowerCase(Locale.ROOT)
        );
    }
}
