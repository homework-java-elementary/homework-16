package com.company.homework16.core.definition;

import com.company.homework16.core.PackageScanner;
import lombok.RequiredArgsConstructor;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class AnnotationBeanDefinitionReader implements BeanDefinitionReader {
    private final List<Class<? extends Annotation>> annotations;
    private final PackageScanner scanner;
    private final String packageName;

    @Override
    public List<BeanDefinition> getBeanDefinitions() {
        return annotations.stream()
                .map(a -> scanner.findWithAnnotation(packageName, a))
                .flatMap(Collection::stream)
                .map(BeanDefinition::create)
                .collect(Collectors.toList());
    }
}
