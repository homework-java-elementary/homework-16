package com.company.homework16.core.definition;

import com.company.homework16.core.PackageScanner;
import com.company.homework16.core.annotation.Bean;
import com.company.homework16.core.annotation.Configuration;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class ConfigurationBeanDefinitionReader implements BeanDefinitionReader {
    private final PackageScanner scanner;
    private final String packageName;

    @Override
    public List<BeanDefinition> getBeanDefinitions() {
        return scanner.findWithAnnotation(packageName, Configuration.class).stream()
                .map(Class::getDeclaredMethods)
                .flatMap(Arrays::stream)
                .filter(m -> m.isAnnotationPresent(Bean.class))
                .map(BeanDefinition::create)
                .collect(Collectors.toList());
    }
}
