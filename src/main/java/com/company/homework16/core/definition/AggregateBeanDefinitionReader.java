package com.company.homework16.core.definition;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AggregateBeanDefinitionReader implements BeanDefinitionReader {
    private final BeanDefinitionReader[] beanDefinitionReaders;

    public AggregateBeanDefinitionReader(BeanDefinitionReader... beanDefinitionReaders) {
        this.beanDefinitionReaders = beanDefinitionReaders;
    }

    @Override
    public List<BeanDefinition> getBeanDefinitions() {
        return Arrays.stream(beanDefinitionReaders)
                .map(BeanDefinitionReader::getBeanDefinitions)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }
}
