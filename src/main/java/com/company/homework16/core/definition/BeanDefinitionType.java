package com.company.homework16.core.definition;

public enum BeanDefinitionType {
    FACTORY_METHOD, CONSTRUCTOR
}
