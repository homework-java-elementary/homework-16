package com.company.homework16.core.postprocess;

import com.company.homework16.core.Context;
import com.company.homework16.core.annotation.Component;
import com.company.homework16.core.annotation.Value;

import java.util.Arrays;

@Component
public class PropertyBeanPostProcess implements BeanPostProcessor {

    @Override
    public void postProcess(Context context, Object bean) {
        Arrays.stream(bean.getClass().getDeclaredFields())
                .filter(filed -> filed.isAnnotationPresent(Value.class))
                .forEach(field -> {
                    Value value = field.getAnnotation(Value.class);
                    String property = System.getProperty(value.value());
                    field.setAccessible(true);
                    try {
                        field.set(bean, property);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }
}
