package com.company.homework16.core.postprocess;

import com.company.homework16.core.Context;

public interface BeanPostProcessor {
    void postProcess(Context context, Object bean);
}
