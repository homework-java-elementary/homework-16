package com.company.homework16.service;

import com.company.homework16.entity.User;
import com.company.homework16.core.annotation.Component;
import com.company.homework16.core.db.JDBCTemplate;
import lombok.RequiredArgsConstructor;

import java.sql.SQLException;
import java.util.List;

@Component
@RequiredArgsConstructor
public class UserService {
    private final JDBCTemplate jdbcTemplate;

    public List<User> getUsers() {
        return jdbcTemplate.query(
                "select * from users",
                new Object[]{},
                (r -> {
                    try {
                        return User.builder()
                                .id(r.getInt("id"))
                                .login(r.getString("login"))
                                .password(r.getString("password"))
                                .build();
                    } catch (SQLException throwables) {
                        return null;
                    }
                }
                )
        );
    }

    public boolean addUser(String login, String password) {
        return jdbcTemplate.query(
                "insert into users(login, password) values(?,?)",
                new Object[]{login, password}
        );
    }
}
