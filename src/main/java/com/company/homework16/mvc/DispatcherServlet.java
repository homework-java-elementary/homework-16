package com.company.homework16.mvc;

import com.company.homework16.core.ApplicationInitializer;
import com.company.homework16.core.Context;
import com.company.homework16.core.PackageScanner;
import com.company.homework16.core.annotation.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

@WebServlet(urlPatterns = "/", name = "DispatcherServlet")
public class DispatcherServlet extends HttpServlet {
    private Context context;

    public DispatcherServlet() {
        PackageScanner packageScanner = new PackageScanner();
        ApplicationInitializer applicationInitializer = new ApplicationInitializer(packageScanner);
        this.context = applicationInitializer.createContext("com.company.homework16");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Object> controllers = context.getBeanByAnnotation(Controller.class);
        String requestMethod = req.getMethod().toUpperCase(Locale.ROOT);

        for (Object controller : controllers) {
            for (Method method : controller.getClass().getDeclaredMethods()) {
                if (!method.isAnnotationPresent(RequestMapping.class)) continue;
                RequestMapping mapping = method.getAnnotation(RequestMapping.class);
                if (!requestMethod.equals(mapping.method().toString())) continue;
                if (!mapping.url().equals(req.getServletPath())) continue;
                try {
                    method.invoke(controller, req, resp);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                return;
            }
            super.service(req, resp);
        }
    }
}
