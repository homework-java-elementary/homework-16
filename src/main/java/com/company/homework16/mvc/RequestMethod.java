package com.company.homework16.mvc;

import java.util.Locale;

public enum RequestMethod {
    GET, POST;

    @Override
    public String toString() {
        return name().toUpperCase(Locale.ROOT);
    }
}
